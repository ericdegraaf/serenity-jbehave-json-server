module.exports = function() {
  var data = {};

  data.customers = [];

  data.customers.push(
      { "id": 1, "initials": "D.E.", "name": "Tester", "address": { "street": "testdrive", "number": 5, "zipcode": "1111AA", "city": "Utrecht"}}
  );

  return data;
}
